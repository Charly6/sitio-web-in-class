// obtenemos el año actual
const anioActual = new Date();
//obtenemos la propiedad del elemento con Id anio
const anio = document.querySelector('#anio');
console.log(anio);
/* 
obtenemos la propiedad textContent para colocar un texto,
y obtenemos el año actual del objeto Date y luego llamamos el metodo getFullYear()
*/
anio.textContent = anioActual.getFullYear();
