const hamburguer = document.querySelector('.hamburguer');

const menu = document.querySelector('.menu-navegacion');

console.log(hamburguer);
console.log(menu);

//adicionar una clase css dinammicamente al html
/** investigar keypress keydown keydown*/
hamburguer.addEventListener('click', () => {
  /** classlist accede a la lista de clases de un elemento toggle cuando solo hay un argumento presente alterna el valor de la clase por ejemplo: si la clase existe la elimina y devuelve false, sino la añade u devuelve true */
  menu.classList.toggle('spread');
});

/** se ejecuta cuando se de click en cualquier parte del navegador */
window.addEventListener('click', (e) => {
  console.log(e.target)
})
