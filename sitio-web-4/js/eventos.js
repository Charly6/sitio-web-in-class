const anioActual = new Date().getFullYear();
const anio = document.querySelector('#anio');
anio.textContent = anioActual;

const hamburguer = document.querySelector('.hamb');
const nav = document.querySelector('header .container nav');
// console.log(nav);
const hambIcono = document.querySelector('.hamb i');
const navLink = document.querySelectorAll('header .container nav a');

hamburguer.addEventListener('click', (e) => {
  console.log(e.target);
  // cancela el evento relacionado
  e.preventDefault();

  nav.classList.toggle('open');
  // cambia el icono de hamb (menu)
  hambIcono.classList.toggle('bi-x');
});

navLink.forEach(enlace=> {
  enlace.addEventListener('click', () => {
    nav.classList.toggle('open')
    hambIcono.classList.toggle('bi-x');
  })
})
