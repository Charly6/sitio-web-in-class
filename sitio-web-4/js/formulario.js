const formulario = document.querySelector('#formulario-contacto');
const inputs = document.querySelectorAll('#formulario-contacto input');
const textarea = document.querySelector('#formulario-contacto textarea');

const expresiones = {
  nombre: /^[a-zA-ZñÑ\s]+$/,
  email: /^(([^<>()\[\]\.,;:\s@\”]+(\.[^<>()\[\]\.,;:\s@\”]+)*)|(\”.+\”))@(([^<>()[\]\.,;:\s@\”]+\.)+[^<>()[\]\.,;:\s@,})$/,\”]{2,})$/,
  mensaje: /^[0-9a-zA-ZñÑ\s]+$/
};

const campos = {
  nombre: false,
  email: false,
  mensaje: false
};

const validarFormularioInput = e => {
  console.log(e.target.id);

  switch(e.target.id){
    case 'nombre':
      validarCampoInput(expresiones.nombre, e.target, 'nombre');
      break;
    case 'email':
      validarCampoInput(expresiones.email, e.target, 'email');
      break;
  }
};

const validarCampoInput = (expresion, input, campo) => {
  // hacemos la evaluacion de la expresion regular mediante el value
  if(expresion.test(input.value)){
    //remueve la clase
    document.querySelector(`#grupo-${campo} .form-control`).classList.remove('form-control-error');
    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.remove('formulario-input-error-activo');
    campos[campo] = true
  }else {
    //añade la clase
    document.querySelector(`#grupo-${campo} .form-control`).classList.add('form-control-error');
    document.querySelector(`#grupo-${campo} .formulario-input-error`).classList.add('formulario-input-error-activo');
    campos[campo] = false
  }
}

const validarFormularioTextArea = e => {
  if(expresiones.mensaje.test(e.target.value)){
    document.querySelector('#grupo-mensaje #mensaje').classList.remove('form-control-error');
    document.querySelector('#grupo-mensaje .formulario-input-error').classList.remove('formulario-input-error-activo');
    campos['mensaje'] = true;
  } else {
    document.querySelector('#grupo-mensaje #mensaje').classList.add('form-control-error');
    document.querySelector('#grupo-mensaje .formulario-input-error').classList.add('formulario-input-error-activo');
    campos['mensaje'] = false;
  }
}

inputs.forEach(input => {
  //console.log(input);
  /* cuando se presiona la tecla y se levanta el dedo se ejecuta el evento */
  input.addEventListener('keyup', validarFormularioInput)
  //se dispara cuando un elemento ha perdido su foco(cuando se cambia de elemento)
  input.addEventListener('blur', validarFormularioInput)
})

textarea.addEventListener('keyup', validarFormularioTextArea)
textarea.addEventListener('blur', validarFormularioTextArea)

formulario.addEventListener('submit', e => {
  e.preventDefault();
  console.log('ingreso al boton')
  if(campos.nombre && campos.mensaje && campos.email){
    let nombre = document.querySelector('#nombre');
    const email = document.querySelector('#email')
    let mensaje = document.querySelector('#mensaje')

    nombre = nombre.value.trim();
    mensaje = mensaje.value.trim();

    console.log(nombre);
    console.log(mensaje);

    console.log("enviando");

    document.querySelector('.formulario-mensaje-exito').classList.add('formulario-mensaje-exito-activo')

    formulario.reset();

    setTimeout(() => {
      document.querySelector('.formulario-mensaje-exito').classList.remove('formulario-mensaje-exito-activo')
    }, 3000);
  }
})